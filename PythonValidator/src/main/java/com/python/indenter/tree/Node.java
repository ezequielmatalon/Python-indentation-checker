package com.python.indenter.tree;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Node {

	private static final Set<String> keywords = new HashSet<>();

	static {
		keywords.add("if");
		keywords.add("while");
		keywords.add("class");
		keywords.add("elif");
		keywords.add("def");
		keywords.add("try");
		keywords.add("except");
		keywords.add("for");
		keywords.add("with");

	}
	private List<Node> childNodes = new ArrayList<Node>();
	private Boolean canBeParent = false;
	private Node parent;
	private final Integer indentation;
	private String line;

	public static Node createTree(String fileName) {
		Node root = new Node("root", -1);
		Node lastNode = root;
		Boolean quota = false;
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {

			String line;
			while ((line = br.readLine()) != null) {
				Boolean hasQuote = line.contains("\"\"\"");
				if (hasQuote) {
					quota ^= true;
					continue;
				}
				if (quota) {
					continue;
				}

				lastNode = lastNode.addNode(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return root;
	}

	public Node(String line, Integer ind) {
		indentation = ind;
		this.line = line;
	}

	private Node(String line, Integer ind, Node par) {
		indentation = ind;
		this.line = line;
		parent = par;
		for (String string : keywords) {
			if (line.contains(string)) {
				canBeParent = true;
				break;
			}
		}
	}

	public Boolean isValidIdentation() {
		if (childNodes.isEmpty()) {
			return true;
		}
		final Integer childInd = childNodes.get(0).indentation;

		return childNodes.stream().allMatch(node -> this.indentation < node.indentation && node.indentation == childInd
				&& node.isValidIdentation() && (node.canBeParent || node.childNodes.isEmpty()));
	}

	public Node addNode(String line) {
		if (line.trim().isEmpty()) {
			return this;
		}
		Integer size = indentationSize(line);
		Node childNode = null;
		if (indentation < size) {
			childNode = new Node(line, size, this);
			childNodes.add(childNode);
		} else {
			if (parent != null)
				childNode = parent.addNode(line);
		}
		return childNode;
	}

	public Integer indentationSize(String line) {
		Integer count = 0;
		for (int i = 0; i < line.length(); i++) {
			if (Character.isWhitespace(line.charAt(i))) {
				count++;
			} else if (line.charAt(i) == '\t') {
				count += 4;
			} else {
				return count;
			}
		}
		return count;
	}

	public void printTree() {
		String ind = "";
		for (int i = 0; i < indentation; i++) {
			ind += " ";
		}
		String padre = "";
		if (!childNodes.isEmpty()) {
			padre = "(parent)";
		}

		String hijo = "";
		if (parent != null) {

			hijo = "(child of " + parent.line.trim().substring(0, parent.line.trim().length()) + " ) ";
		}
		System.out.println(hijo + padre + ind + line + " ident=" + this.indentation);
		childNodes.forEach(c -> c.printTree());
	}

	public void printChilds() {
		for (Node node : childNodes) {

			String ind = "";
			for (int i = 0; i < node.indentation; i++) {
				ind += " ";
			}
			System.out.println(ind + node.line + " ident=" + node.indentation);
		}
	}
}
