package com.python.indenter.tree;

import org.junit.Before;

import org.junit.*;

public class NodeTest {

	private Node wellformedTree;
	private Node malformedTree;

	@Before
	public void init() {
		ClassLoader cl = this.getClass().getClassLoader();
		wellformedTree = Node.createTree(cl.getResource("python.py").getPath());
		malformedTree = Node.createTree(cl.getResource("python2.py").getPath());
	}

	@Test
	public void indentedFile() {
		Assert.assertTrue(wellformedTree.isValidIdentation());
	}

	@Test
	public void malformedFile() {
		Assert.assertFalse(malformedTree.isValidIdentation());
	}

	@Test
	public void print() {
		wellformedTree.printTree();
	}
}
